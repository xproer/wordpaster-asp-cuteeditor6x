<!-- #include file = "cuteeditor_files/include_CuteEditor.asp" --> 
<html>	
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WordPaster For CuteEditor6.x</title>
		<link rel="stylesheet" href="asp.css"  type="text/css" />
        <link type="text/css" rel="Stylesheet" href="demo.css" />
	<script type="text/javascript" src="WordPaster/js/jquery-1.8.3.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/layer-3.1.1/layer.js" charset="utf-8"></script>
	<script type="text/javascript" src="WordPaster/js/w.layer.js" charset="utf-8"></script>
	<script type="text/javascript" src="demo.js" charset="utf-8"></script>
	</head>
    <body>
		
		<form name="theForm" action="Get_HtmlContent.asp" method="post">
		<h1>Enable All Toolbars</h1>
		<hr>
	<div style="font-size: medium; line-height: 130%;">
		<p>
			演示方法：</p>
		<ul style="list-style-type: decimal;">
			<li>打开Word文档，复制多张图片，在编辑器中按下 Ctrl + V 键，编辑器将自动上传所有图片。</li>
			<li>复制电脑中任意图片文件，然后点击编辑器中的粘贴图片按钮<img src="WordPaster/css/paster.png" width="16px" height="16px" />。</li>
			<li>通过QQ或其它软件截屏，复制图片，然后点击编辑器中的图片粘贴按钮<img src="WordPaster/css/paster.png" width="16px" height="16px" />。</li>
		</ul>
		<p>
			相关问题：</p>
		<ul style="list-style-type: decimal;">
			<li>安装插件<a target="_blank" href="http://www.ncmem.com/webapp/wordpaster/pack.aspx">下载插件</a></li>
            <li>WindowxXP/Windows7/Windows2003如果无法识别数字证书，请先下安装<a target="_blank" href="http://www.ncmem.com/download/WoSignRootUpdate.rar">数字签名根证书</a></li>
		</ul>
	</div>		<br />
		<%
			Dim editor
			Set editor = New CuteEditor
			editor.ID = "Editor1"
			editor.Text = "WordPaster For CuteEditor6.6整合示例"
			
			editor.EditorBodyStyle = "font:normal 12px arial;"
			editor.EditorWysiwygModeCss = "asp.css"
			editor.CustomAddons = "<img class=""CuteEditorButton"" onmouseover=""CuteEditor_ButtonCommandOver(this)"" onmouseout=""CuteEditor_ButtonCommandOut(this)"" onmousedown=""CuteEditor_ButtonCommandDown(this)"" onmouseup=""CuteEditor_ButtonCommandUp(this)"" Command=""WordPaster"" src=""WordPaster/css/paster.png"" title=""Word一键粘贴"" />"   
			editor.Draw()
												
			' Request.Form(ID) access from other page
		%>
		<div id="demos"></div>
		<script type="text/javascript">
			var pos = window.location.href.lastIndexOf("/");
			var api = [
				window.location.href.substr(0, pos + 1),
				"asp/upload.asp"
			].join("");
			WordPaster.getInstance({
				//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
				PostUrl: api,
				//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
				ImageUrl: "",
				//设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
				FileFieldName: "file",
				//提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
				ImageMatch: ''				
			});//加载控件
		
            var edt = document.getElementById('<%= editor.ClientID%>');
    
            $(document).ready(function()
            {
				WordPaster.getInstance().SetEditor(edt);
            });
			
			function CuteEditor_OnCommand(editor,command,ui,value)    
			{    
				//handle the command by yourself    
				if(command=="WordPaster")
				{    
					WordPaster.getInstance().Paste();
					return true;    
				}    
			}    
        </script>
		</form>
	</body>
</html>