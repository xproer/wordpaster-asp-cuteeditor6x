$(function () {
    var demos = [
        {
            name: "示例：",
            pages: [
                {
                    name: "skygqbox示例",
                    url: "default.asp"
                },
                {
                    name: "jqueryui弹窗示例",
                    url: "default-jqueryui.asp"
                },
                {
                    name: "layer弹窗示例",
                    url: "default-layer.asp"
                }
            ]
        }
    ];

    //url=>res/
    //http://localhost:8888/filemgr/res/up6/up6.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("demo.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("demo.js"));
            }
        }
        return jsPath;
    };
    //http://localhost/res/down2/
    var root = this.getJsDir();
    $(demos).each(function (i, n) {
        var tmps = ["<ul class='list-hor'>", "<li><b>" + n.name + "</b></li>"];
        $(n.pages).each(function (i, n) {
            tmps.push("<li><a target='_blank' href='" + root + n.url + "'>" + n.name + "</a></li>");
        });
        tmps.push("</ul>");
        $("#demos").append(tmps.join(""));
    });
});